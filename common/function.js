const bcrypt = require('bcrypt');

const generateDefaultPassword = async () => {
	const salt = await bcrypt.genSalt(10);
	const hashPassword = await bcrypt.hash('1111', salt);

	return hashPassword;
};

const generateHash = async (password) => {
	const salt = await bcrypt.genSalt(10);
	const hashPassword = await bcrypt.hash(password, salt);

	return hashPassword;
};

const validPassword = async (password, hash) => {
	const checkValid = await bcrypt.compare(password, hash);

	return checkValid;
};

const autoGenerateUsername = (fullname) => {
	const split = fullname
		.normalize('NFD')
		.replace(/[\u0300-\u036f]/g, '')
		.replace(/đ/g, 'd')
		.replace(/Đ/g, 'D')
		.trim()
		.replace(/\s\s+/g, ' ')
		.toLowerCase()
		.split(' ');

	let result = split[split.length - 1];
	for (let i = 0; i < split.length - 1; i++) {
		result += split[i].charAt(0);
	}

	return (
		result +
		(Math.floor(Math.random() * (1000000 - 100000)) + 100000)
	).toString();
};

function getDays(date, day) {
	var d = new Date(date),
		month = d.getMonth(),
		days = [];

	d.setDate(d.getDate());

	while (d.getDay() !== day) {
		d.setDate(d.getDate() + 1);
	}

	while (d.getMonth() === month) {
		days.push(new Date(d.getTime()));
		d.setDate(d.getDate() + 7);
	}

	return days;
}

module.exports = {
	generateDefaultPassword,
	generateHash,
	validPassword,
	autoGenerateUsername,
	getDays,
};
