const pool = require('../configs/db.config');
const HttpError = require('../models/http-error');
const { validationResult } = require('express-validator');

const getUserInfo = async (req, res, next) => {
	const userId = req.userData.userId;
	try {
		const query = `
		select 
			full_name,
			email,
			phone,
			note 
		from 
			users
		where user_id = $1
		`;
		const params = [userId];
		const data = await pool.query(query, params);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getCourseList = async (req, res, next) => {
	const { q, subject_id, page, limit, is_buy } = req.query;
	const offset = (page - 1) * limit;
	const userId = req.userData.userId;
	const role = req.userData.role;
	try {
		if (is_buy == '0') {
			const query = `
            Select  distinct a.course_id as id,b.title,b.subject_name, b.cost as course_cost,								
                    b.percent, b.teacher_name,number_student, b.status as course_status, b.brief_info,b.created_at,b.subject_id,
                    coalesce(c.number_lesson,0)	as number_lesson,a.thumbnail,a.discount,course_student.join_date	
                    from(								
                        select 
                            count(student_id) as number_student, course.course_id,course.thumbnail,course.discount				
                        from 
                            course								
                        left join 
                            course_student on course.course_id = course_student.course_id
                        where course.status ='1'								
                        group by 
                            course.course_id								
                    )a INNER JOIN(								
                        select 
                            course.course_id,course.title,subject.subject_name as subject_name, course.cost ,							
                            course.percent, users.full_name as teacher_name, course.status , course.brief_info, 
                            course.flag, subject.subject_id, course.created_at				
                        from 
                            course 
                        INNER JOIN subject ON course.subject_id = subject.subject_id	
                        INNER JOIN users on users.user_id = course.created_by
                        where course.status ='1'	
                    ) b on a.course_id = b.course_id	
                        left join (
                        select count(lesson_id) as number_lesson,course_id from lesson
                                    group by course_id
                        ) c on a.course_id = c.course_id
                    left join course_student
						on course_student.course_id = a.course_id and student_id = $5                            
                   where    b.flag=false and 
                            LOWER(b.title) like LOWER($1) 
                            and b.subject_id  ${
															subject_id === '' ? '!' : ''
														}= $2							
                    group by 
                            a.course_id,b.title,b.subject_id, b.cost ,								
                            b.percent, b.teacher_name,a.number_student,
                            b.status,b.brief_info,b.subject_id, 
                            b.created_at,b.subject_name,c.number_lesson,
                            a.thumbnail,a.discount,course_student.join_date			
                    order by 
                   b.created_at DESC  LIMIT $3 OFFSET $4
            `;
			const param = ['%' + q + '%', subject_id, limit, offset, userId];
			const data = await pool.query(query, param);
			const result = {
				data: data.rows,
			};
			return res.status(200).json(result);
		} else {
			const query = `
       							
                        select 
							course.course_id as id,course.title,course.thumbnail,course.brief_info,
							course.discount,course.cost as course_cost,course_student.join_date
                        from 
                            course 
                        INNER JOIN subject ON course.subject_id = subject.subject_id	
                        INNER JOIN users on users.user_id = course.created_by
                        left JOIN course_student on course_student.course_id = course.course_id
                        where course.status ='1' and  course_student.student_id =$5
							and course.flag=false and join_date is not null and
                            LOWER(course.title) like LOWER($1) 
                            and course.subject_id  ${
															subject_id === '' ? '!' : ''
														}= $2								
                    order by course_student.join_date  LIMIT $3 OFFSET $4
            `;
			const param = ['%' + q + '%', subject_id, limit, offset, userId];
			const data = await pool.query(query, param);
			const result = {
				data: data.rows,
			};
			return res.status(200).json(result);
		}
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getLessonList = async (req, res, next) => {
	const { course_id } = req.query;

	const userId = req.userData.userId;
	const role = req.userData.role;
	try {
		const query_check_course_student = `
            select * 
            from 
                course_student 
            where 
                student_id=$1 and course_id=$2 and flag='false'
        `;
		const params_check_course_student = [userId, course_id];
		const check_course_student = await pool.query(
			query_check_course_student,
			params_check_course_student
		);

		const query = `
		select lesson_source.order ,lesson.lesson_order,lesson.lesson_id,lesson.lesson_title,
                source.source_title,source.source_duration, source.source_duration, 
                source.source_link,source.source_detail,source.source_id,lesson_source.order from lesson 
        inner join
            lesson_source 
        on 
            lesson.lesson_id =lesson_source.lesson_id and lesson_source.flag=false  and lesson_source.status='1'
        inner join source  
        on 
            lesson_source.source_id = source.source_id and source.flag=false 
        where course_id = $1 
        order by lesson.lesson_order , lesson_source.order 
		`;
		const param = [course_id];
		const response = await pool.query(query, param);
		const data = response.rows;
		const list_lesson = [];
		let list_source = [];
		let lesson_id = '';
		for (let i = 0; i < data.length; i++) {
			if (lesson_id != data[i].lesson_id) {
				if (list_source.length > 0) {
					const lesson = {
						lesson_id: data[i - 1].lesson_id,
						lesson_title: data[i - 1].lesson_title,
						list_source: list_source,
					};
					list_lesson.push(lesson);
				}

				lesson_id = data[i].lesson_id;
				list_source = [];
				if (check_course_student.rowCount == '1') {
					list_source.push({
						source_order: data[i].order,
						source_id: data[i].source_id,
						source_detail: data[i].source_detail,
						source_duration: data[i].source_duration,
						source_link: data[i].source_link,
						source_title: data[i].source_title,
					});
				} else {
					list_source.push({
						source_order: data[i].order,
						source_id: '',
						source_detail: '',
						source_duration: data[i].source_duration,
						source_link: '',
						source_title: data[i].source_title,
					});
				}
			} else {
				if (check_course_student.rowCount == '1') {
					list_source.push({
						source_order: data[i].order,
						source_id: data[i].source_id,
						source_detail: data[i].source_detail,
						source_duration: data[i].source_duration,
						source_link: data[i].source_link,
						source_title: data[i].source_title,
					});
				} else {
					list_source.push({
						source_order: data[i].order,
						source_id: '',
						source_detail: '',
						source_duration: data[i].source_duration,
						source_link: '',
						source_title: data[i].source_title,
					});
				}
				// list_source.push({
				//     source_order:data[i].order,
				//     source_id:data[i].source_id,
				//     source_detail:data[i].source_detail,
				//     source_duration:data[i].source_duration,
				//     source_link:data[i].source_link,
				//     source_title:data[i].source_title
				// })
			}
		}
		if (data.length > 0) {
			const lesson = {
				lesson_id: data[data.length - 1].lesson_id,
				lesson_title: data[data.length - 1].lesson_title,
				list_source: list_source,
			};
			list_lesson.push(lesson);
		}

		const result = {
			data: list_lesson,
			total_lesson: data.length,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const checkIsBuy = async (req, res, next) => {
	const { course_id } = req.query;
	const userId = req.userData.userId;

	try {
		const query = `
			select * from course_student where course_id =$1 and join_date is not null and student_id=$2 and flag=false
		`;
		const params = [course_id, userId];

		const data = await pool.query(query, params);
		if (data.rowCount <= 0) {
			return res.status(200).json({ messsage: false });
		} else {
			return res.status(200).json({ messsage: true });
		}
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const sendFeedBack = async (req, res, next) => {
	const userId = req.userData.userId;
	const role = req.userData.role;
	const { courseId, feedback } = req.body;

	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return next(
			new HttpError(
				'Đầu vào không hợp lệ, vui lòng kiểm tra dữ liệu của bạn.',
				422
			)
		);
	}

	if (role !== '5') {
		const error = new HttpError('Bạn không có quyền truy cập trang này.', 403);
		return next(error);
	}

	let checkPermiss;
	try {
		const query = `SELECT course_id FROM course_student WHERE course_id = $1 AND student_id = $2`;
		const params = [courseId, userId];

		checkPermiss = await pool.query(query, params);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);

		return next(error);
	}

	if (checkPermiss.rowCount < 1) {
		const error = new HttpError('Bạn không có quyền truy cập trang này', 403);
		return next(error);
	}

	try {
		const query = `INSERT INTO feedback(
			feedback_id, writer_id, course_id, feedback, status)
			VALUES (nextval('feedback_id'), $1, $2, $3, $4)`;
		const params = [userId, courseId, feedback, '0'];

		await pool.query(query, params);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	}

	return res.status(200).json({ message: 'Gửi phản hồi thành công' });
};

module.exports = {
	getUserInfo,
	getCourseList,
	getLessonList,
	sendFeedBack,
	checkIsBuy,
};
