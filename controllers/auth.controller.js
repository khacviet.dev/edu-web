require('dotenv').config();
const { generateHash } = require('../common/function');

const { validationResult } = require('express-validator');
const { v4: uuidv4 } = require('uuid');
const jwt = require('jsonwebtoken');
const passport = require('passport');

const HttpError = require('../models/http-error');
const pool = require('../configs/db.config');
const mail = require('../configs/mail.config');

const signup = async (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return next(
			new HttpError('Invalid inputs passed, please check your data.', 422)
		);
	}

	const { fullname, username, password, email, phone } = req.body;

	const client = await pool.connect();

	try {
		await client.query('BEGIN');
		let user;

		const userQuery = 'SELECT user_id FROM users WHERE username = $1 LIMIT 1';
		const userParams = [username];

		user = await pool.query(userQuery, userParams);

		if (user.rowCount > 0) {
			const error = new HttpError('Tài khoản đã tồn tại', 422);
			return next(error);
		}

		let checkEmail;

		const checkEmailQuery =
			'SELECT user_id FROM users WHERE email = $1 LIMIT 1';
		const checkEmailParams = [email];

		checkEmail = await pool.query(checkEmailQuery, checkEmailParams);

		if (checkEmail.rowCount > 0) {
			const error = new HttpError('Địa chỉ email đã tồn tại', 422);
			return next(error);
		}

		const passwordHash = await generateHash(password);

		const insertUserQuery = `INSERT INTO users(											
			user_id, username, password, role_id, full_name, display_name, email, created_by, modified_by, type, status, avt_link, phone, address, gender, note, is_confirm)										
			VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13,$14, $15, $16, $17)`;
		const insertUserParams = [
			username,
			username,
			passwordHash,
			'5',
			fullname,
			username,
			email,
			username,
			username,
			'1',
			'1',
			'https://i.pravatar.cc',
			phone,
			'',
			'male',
			'',
			false,
		];

		await pool.query(insertUserQuery, insertUserParams);

		const randomCode = Math.floor(Math.random() * (1000000 - 100000)) + 100000;

		const insertCodeQuery =
			'INSERT INTO users_confirm_code(user_confirm_code_id, user_id, confirm_code) VALUES ($1, $2, $3)';
		const insertCodeParams = [uuidv4(), username, randomCode];

		await pool.query(insertCodeQuery, insertCodeParams);

		const mailOptions = {
			from: 'Xoài Academy <vietokokbusiness@gmail.com>',
			to: email.toString(),
			subject: `Xác nhận địa chỉ email của bạn trên Xoài Education`,
			text: '',
			html: `<html>
			<head>
				<style>
					p {
						font-family: Arial, Helvetica, sans-serif
					}
					div {
						font-family: Arial, Helvetica, sans-serif
					}
					.button {
						background-color: #008CBA;
						border: none;
						color: white;
						padding: 15px 32px;
						text-align: center;
						text-decoration: none;
						display: inline-block;
						font-size: 16px;
						margin: 4px 2px;
						cursor: pointer;
					}				 
					  a {
						text-decoration: none !important;
						color: #fff !important;
					}				
				</style>
			</head>
			<body>
				<div>
					<h3>Xác nhận địa chỉ email của bạn để trải nghiệm các dịch vụ của Xoài Education</h3>
					<button class="button"><a href="https://xoaieducation.site/confirm?email=${email.toString()}&code=${randomCode.toString()}">Xác nhận địa chỉ email</a></button>				
				</div>
			</body>
			</html>`,
		};

		await mail.sendMail(mailOptions);

		await client.query('COMMIT');
	} catch (err) {
		await client.query('ROLLBACK');
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	} finally {
		mail.close();
		client.release();
	}

	return res.status(201).json({ message: 'Đăng ký thành công!' });
};

const signin = async (req, res, next) => {
	passport.authenticate('local', async (err, user, info) => {
		try {
			if (err || !user) {
				const error = new HttpError(
					'Tài khoản hoặc mật khẩu không đúng, vui lòng thử lại.',
					401
				);
				return next(error);
			}

			req.login(user, { session: false }, async (error) => {
				if (error) return next(error);

				const userId = user.userId;
				const role = user.role;

				const accessToken = jwt.sign(
					{ userId, role },
					process.env.ACCESS_TOKEN_SERCRET_KEY,
					{
						expiresIn: '7d',
					}
				);

				res.cookie('access_token', accessToken, {
					maxAge: 24 * 60 * 60 * 7 * 1000,
					httpOnly: true,
				});

				res.cookie('c_user', userId, {
					maxAge: 24 * 60 * 60 * 7 * 1000,
				});

				res.cookie('c_role', role, {
					maxAge: 24 * 60 * 60 * 7 * 1000,
				});

				return res.status(200).json({ message: 'Đăng nhập thành công' });
			});
		} catch (error) {
			return next(error);
		}
	})(req, res, next);
};

const logout = (req, res) => {
	res.clearCookie('access_token');
	res.clearCookie('c_user');
	res.clearCookie('c_role');
	res.clearCookie('course_cart');

	return res.status(200).json({
		logout: true,
	});
};

const forgotPassword = async (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return next(
			new HttpError('Invalid inputs passed, please check your data.', 422)
		);
	}

	const { username } = req.body;
	let user;

	try {
		const query = `SELECT user_id, full_name FROM users WHERE email = $1 AND status='1' LIMIT 1`;
		const params = [username];

		user = await pool.query(query, params);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	}

	if (user.rowCount === 0) {
		const error = new HttpError('Tài khoản không tồn tại', 404);
		return next(error);
	}

	const randomCode = Math.floor(Math.random() * (1000000 - 100000)) + 100000;

	try {
		const query =
			'INSERT INTO users_recover_code(code_id, code_value, user_id, expire_date) VALUES ($1, $2, $3, $4)';
		const params = [
			uuidv4(),
			randomCode,
			user.rows[0].user_id,
			new Date(new Date().getTime() + 180 * 1000),
		];

		await pool.query(query, params);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	}

	const mailOptions = {
		from: 'Xoài Academy <vietokokbusiness@gmail.com>',
		to: username.toString(),
		subject: `${randomCode.toString()} là mã khôi phục tài khoản Xoài Academy của bạn`,
		text: '',
		html: `<html>
		<head>
			<style>
				p {
					font-family: Arial, Helvetica, sans-serif
				}
				div {
					font-family: Arial, Helvetica, sans-serif
				}
				.code {
					font-size: 2.5rem;
				}
			</style>
		</head>
		<body>
			<div>
				<p>Hi ${user.rows[0].full_name},<br/></p>
				<p>Chúng tôi đã nhận được yêu cầu đặt lại mật khẩu Xoài Academy của bạn.<br/>Nhập mã đặt lại mật khẩu sau:</p>
				<div class="code">${randomCode.toString()}</div>
			</div>
		</body>
		</html>`,
	};

	try {
		await mail.sendMail(mailOptions);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	} finally {
		mail.close();
	}

	return res.status(200).json({ userId: user.rows[0].user_id, username });
};

const checkRecoverCode = async (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return next(
			new HttpError('Invalid inputs passed, please check your data.', 422)
		);
	}
	const userId = req.params.userId;
	const { code } = req.body;
	let user;

	try {
		const query = `SELECT user_id FROM users WHERE user_id = $1 AND status='1' LIMIT 1`;
		const params = [userId];

		user = await pool.query(query, params);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	}

	if (user.rowCount === 0) {
		const error = new HttpError('Tài khoản không tồn tại', 404);
		return next(error);
	}

	let recoverCode;
	try {
		const query = `SELECT * FROM users_recover_code INNER JOIN users ON users.user_id = users_recover_code.user_id WHERE code_value = $1 AND users.user_id = $2 AND users.status = '1'`;
		const params = [code, userId];

		recoverCode = await pool.query(query, params);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	}

	if (recoverCode.rowCount === 0) {
		const error = new HttpError('Bạn đã nhập sai mã, vui lòng nhập lại', 404);
		return next(error);
	}

	if (
		new Date(recoverCode.rows[0].expire_date).getTime() < new Date().getTime()
	) {
		const error = new HttpError('Mã của bạn đã hết hạn', 406);
		return next(error);
	}

	try {
		const query = 'UPDATE users SET is_reset = $1 WHERE user_id = $2';
		const params = [true, userId];

		await pool.query(query, params);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	}

	return res.status(200).json({ message: 'Successfully' });
};

const setNewPassword = async (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return next(
			new HttpError('Invalid inputs passed, please check your data.', 422)
		);
	}
	const userId = req.params.userId;
	const { newpassword } = req.body;
	let user;

	try {
		const query = `SELECT user_id, is_reset FROM users WHERE user_id = $1 AND status='1' LIMIT 1`;
		const params = [userId];

		user = await pool.query(query, params);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	}

	if (user.rowCount === 0) {
		const error = new HttpError('Tài khoản không tồn tại', 404);
		return next(error);
	}

	if (user.rows[0].is_reset === false) {
		const error = new HttpError('Bạn không có quyền cho API này.', 403);
		return next(error);
	}

	const passwordHash = await generateHash(newpassword);

	try {
		const query =
			'UPDATE users SET password = $1, is_reset = $2 WHERE user_id = $3';
		const params = [passwordHash, false, userId];

		await pool.query(query, params);
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	}

	return res.status(200).json({ message: 'Successfully' });
};

const checkConfirmCode = async (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return next(
			new HttpError('Invalid inputs passed, please check your data.', 422)
		);
	}

	const { email, code } = req.body;

	try {
		let check;
		let checkQuery = `SELECT users.user_id, is_confirm FROM users INNER JOIN users_confirm_code ON users.user_id = users_confirm_code.user_id
		WHERE email = $1 AND confirm_code = $2 LIMIT 1`;
		let checkParams = [email.toString(), code.toString()];

		check = await pool.query(checkQuery, checkParams);

		if (check.rowCount < 1) {
			const error = new HttpError('Email hoặc mã xác nhận không đúng', 404);
			return next(error);
		}

		if (check.rows[0].is_confirm === false) {
			const updateQuery = `UPDATE users
		SET modified_by=$1, modified_at=$2, is_confirm=$3
		WHERE email = $4`;
			const updateParam = [
				check.rows[0].user_id,
				new Date(),
				true,
				email.toString(),
			];

			await pool.query(updateQuery, updateParam);
		}

		return res.status(200).json('1');
	} catch (err) {
		const error = new HttpError('Đã có lỗi xảy ra, vui lòng thử lại.', 500);
		return next(error);
	}
};

module.exports = {
	signup,
	signin,
	logout,
	forgotPassword,
	checkRecoverCode,
	setNewPassword,
	checkConfirmCode,
};
