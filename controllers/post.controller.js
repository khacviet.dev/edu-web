const pool = require('../configs/db.config');
const HttpError = require('../models/http-error');

const getFourNewPost = async (req, res, next) => {
	try {
		const query = `
        select 
            users.full_name, post_id,thumbnail,title from post inner join users on 
            users.user_id=post.created_by
        where
            post.flag=false and
			post.status = '1'
        order by
            post.created_at DESC
        offset 0 limit 4
		`;
		const data = await pool.query(query);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getPostCategory = async (req, res, next) => {
	try {
		const query = `
		select
			setting_id as id,setting_value as value 
		from 
			setting 
		where 
			setting_type='post_category' and flag = false`;
		const data = await pool.query(query);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getPosts = async (req, res, next) => {
	const { q, category_id, page, limit } = req.query;
	const offset = (page - 1) * limit;
	try {
		const query = `
		select 
			distinct post.post_id,post.thumbnail,post.title,post.brief_info,post.number_click,post.is_featured,
			to_char(post.created_at,'DD-MM-YYYY') as date
			from post
		inner join post_setting on post.post_id= post_setting.post_id
		where 
			post.status = '1' and
			post.flag=false
			and LOWER(post.title) like LOWER($1) 
			and post_setting.setting_id  ${category_id === '' ? '!' : ''}= $2	
		order by 
			is_featured DESC, date ASC LIMIT $3 OFFSET $4`;
		const params = ['%' + q + '%', category_id, limit, offset];
		const data = await pool.query(query, params);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getPostById = async (req, res, next) => {
	const { post_id } = req.query;

	try {
		const query = `
			
		select 	distinct post.post_id, post.title,to_char(post.created_at,'DD-MM-YYYY') as date,post.number_click,
			post.full_content,post.ebook_link, 
			STRING_AGG(setting.setting_value,',') as category,
			STRING_AGG(setting.setting_id,',') as category_id,
			users.full_name,users.avt_link,users.short_description
		from 
			post
		left join 
			post_setting on post.post_id= post_setting.post_id
		left join 
			setting on setting.setting_id = post_setting.setting_id
		left join 
			users on users.user_id = post.created_by
		where 
			post.post_id=$1
			and post.status ='1'
		group by post.post_id, post.title,users.full_name,users.avt_link,users.short_description`;
		const params = [post_id];
		const data = await pool.query(query, params);

		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getPostDepend = async (req, res, next) => {
	const { category_id } = req.query;

	if (typeof category_id === 'string') {
		try {
			const query = `
				
			select 	distinct post.post_id,post.title,post.number_click
			from 
				post
			left join 
				post_setting on post.post_id= post_setting.post_id
			left join 
				setting on setting.setting_id = post_setting.setting_id
			where 
				setting.setting_id= $1
				and post.status ='1'
				and post.flag= false
			order by post.number_click limit 5 offset 0`;
			const params = [category_id];

			const data = await pool.query(query, params);

			const result = {
				data: data.rows,
			};
			return res.status(200).json(result);
		} catch (err) {
			const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
			return next(error);
		}
	} else {
		try {
			const query = `
				
			select 	distinct post.post_id,post.title,post.number_click
			from 
				post
			left join 
				post_setting on post.post_id= post_setting.post_id
			left join 
				setting on setting.setting_id = post_setting.setting_id
			where 
				setting.setting_id= ANY($1)
				and post.status ='1'
				and post.flag= false
			order by post.number_click limit 5 offset 0`;
			const params = [category_id];

			const data = await pool.query(query, params);

			const result = {
				data: data.rows,
			};
			return res.status(200).json(result);
		} catch (err) {
			const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
			return next(error);
		}
	}
};

const updateClick = async (req, res, next) => {
	const { post_id } = req.body;

	try {
		const query = `
			Select number_click from post where post_id = $1	
		`;
		const params = [post_id];
		const data = await pool.query(query, params);
		const number_click = BigInt(data.rows[0].number_click) + BigInt(1);
		const update_qury = `
		UPDATE public.post
		SET number_click=$1
		WHERE post_id=$2;
		`;
		const params_update = [number_click, post_id];
		await pool.query(update_qury, params_update);
		return res.status(200).json({
			message: 'Ok',
		});
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

module.exports = {
	getFourNewPost,
	getPostCategory,
	getPosts,
	getPostById,
	getPostDepend,
	updateClick,
};
