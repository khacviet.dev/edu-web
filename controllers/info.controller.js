const pool = require('../configs/db.config');
const HttpError = require('../models/http-error');

const getSliders = async (req, res, next) => {
	try {
		const query = `
		    select  slider_id,
                    image_link,
                    redirect_link 
            from 
                slider 
            where 
                flag=false and
                is_show=true
		`;
		const data = await pool.query(query);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getFooter = async (req, res, next) => {
	try {
		// laays thong tin dia chi
		const query = `
		    select  setting_value , description 
            from 
                setting 
            where 
                flag=false and setting_type='education_address'
		`;
		// lay thong tin tu van vien
		const query_get_consultant = `select full_name,email,phone from users where role_id ='2' and type='3' and flag=false`;
		// Lay thong tin gioi thieu ngan ve trung tam
		const query_get_shortDes = `select description from setting where setting_type='setting_education_des' and flag=false`;

		const get_consultant = await pool.query(query_get_consultant);
		const get_shortDes = await pool.query(query_get_shortDes);
		const data = await pool.query(query);

		const result = {
			consultant: get_consultant.rows,
			shortDes: get_shortDes.rows,
			address: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

module.exports = {
	getSliders,
	getFooter,
};
