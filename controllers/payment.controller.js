const pool = require('../configs/db.config');
const HttpError = require('../models/http-error');

const getCourseByCourseCart = async (req, res, next) => {
	const { courseCart } = req.query;

	if (courseCart === undefined) {
		return res.status(200).json([]);
	}

	const courseCartSplit = courseCart.split('-');

	let courses;
	try {
		const query = `SELECT course_id, thumbnail, title, cost, (cost - discount) as cost_new FROM course
		WHERE course_id=ANY($1)`;
		const params = [[courseCartSplit]];

		courses = await pool.query(query, params);

		let total = 0;
		if (courses.rowCount > 0) {
			for (let i = 0; i < courses.rowCount; i++) {
				total += courses.rows[i].cost_new;
			}
		}

		return res.status(200).json({ courses: courses.rows, total });
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const paymentCourse = async (req, res, next) => {
	const userId = req.userData.userId;

	const { courses } = req.body;

	const client = await pool.connect();

	try {
		await client.query('BEGIN');
		for (let i = 0; i < courses.length; i++) {
			let check;
			let query = `SELECT COUNT(*) FROM course_student WHERE course_id = $1 AND student_id = $2`;
			let params = [courses[i].course_id, userId];
			check = await client.query(query, params);
			if (check.rows[0].count > 0) {
				const error = new HttpError(
					'Bạn đã đăng ký một trong những khóa học này, vui lòng kiểm tra lại.',
					409
				);
				return next(error);
			}
		}

		for (let i = 0; i < courses.length; i++) {
			let query = `INSERT INTO course_student(			
				course_id, student_id, created_by, price)		
				VALUES ($1, $2, $3, $4)`;
			let params = [courses[i].course_id, userId, userId, courses[i].cost_new];
			await client.query(query, params);
		}

		let user;
		const queryUser = `select full_name, email, phone, note from users where user_id = $1`;
		const paramUser = [userId];
		user = await client.query(queryUser, paramUser);

		for (let i = 0; i < courses.length; i++) {
			const registerQuery = `INSERT INTO register(						
                register_id, user_id, course_id, full_name, email, phone, status, created_by, note, date, flag)					
                VALUES (nextval('register_id'), $1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`;
			const registerParams = [
				userId,
				courses[i].course_id,
				user.rows[0].full_name,
				user.rows[0].email,
				user.rows[0].phone,
				'2',
				userId,
				user.rows[0].note,
				new Date(),
				false,
			];
			await client.query(registerQuery, registerParams);
		}
		await client.query('COMMIT');
		return res.status(201).json({ message: 'Đăng ký thành công' });
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	} finally {
		client.release();
	}
};

module.exports = {
	getCourseByCourseCart,
	paymentCourse,
};
