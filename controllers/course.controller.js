const pool = require('../configs/db.config');
const HttpError = require('../models/http-error');

const getCourse = async (req, res, next) => {
	const { course_id } = req.query;

	try {
		const query = `
		Select  distinct a.course_id as id,b.title,b.subject_name, b.cost as course_cost,								
                b.percent, b.teacher_name,number_student, b.status as course_status, b.brief_info,b.created_at,b.subject_id,
				coalesce(c.number_lesson,0)	as number_lesson,a.thumbnail,a.discount	,b.description, b.short_description,b.avt_link
                from(								
                    select 
                        count(student_id) as number_student, course.course_id,course.thumbnail,course.discount				
                    from 
                        course								
                    left join 
                        course_student on course.course_id = course_student.course_id								
                    group by 
                        course.course_id								
                )a INNER JOIN(								
                    select 
                        course.course_id,course.title,subject.subject_name as subject_name, course.cost ,							
                        course.percent, users.full_name as teacher_name, course.status , course.brief_info, 
                        course.flag, subject.subject_id, course.created_at	,course.description,
                        users.short_description,users.avt_link
                    from 
                        course 
					INNER JOIN subject ON course.subject_id = subject.subject_id	
					INNER JOIN users on users.user_id = course.created_by
                    where course.flag=false
					
                ) b on a.course_id = b.course_id	
					left join (
					select count(lesson_id) as number_lesson,course_id from lesson
								group by course_id
					) c on a.course_id = c.course_id
												
               where a.course_id = $1 
		`;
		const param = [course_id];
		const data = await pool.query(query, param);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getCourseList = async (req, res, next) => {
	const { q, subject_id, page, limit, is_buy } = req.query;
	const offset = (page - 1) * limit;
	try {
		const query = `
            Select  distinct a.course_id as id,b.title,b.subject_name, b.cost as course_cost,								
                    b.percent, b.teacher_name,number_student, b.status as course_status, b.brief_info,b.created_at,b.subject_id,
                    coalesce(c.number_lesson,0)	as number_lesson,a.thumbnail,a.discount		
                    from(								
                        select 
                            count(student_id) as number_student, course.course_id,course.thumbnail,course.discount				
                        from 
                            course								
                        left join 
                            course_student on course.course_id = course_student.course_id
                        where course.status ='1'								
                        group by 
                            course.course_id								
                    )a INNER JOIN(								
                        select 
                            course.course_id,course.title,subject.subject_name as subject_name, course.cost ,							
                            course.percent, users.full_name as teacher_name, course.status , course.brief_info, 
                            course.flag, subject.subject_id, course.created_at				
                        from 
                            course 
                        INNER JOIN subject ON course.subject_id = subject.subject_id	
                        INNER JOIN users on users.user_id = course.created_by
                        where course.status ='1'	
                    ) b on a.course_id = b.course_id	
                        left join (
                        select count(lesson_id) as number_lesson,course_id from lesson
                                    group by course_id
                        ) c on a.course_id = c.course_id
                                                    
                   where    b.flag=false and 
                            LOWER(b.title) like LOWER($1) 
                            and b.subject_id  ${
															subject_id === '' ? '!' : ''
														}= $2							
                    group by 
                            a.course_id,b.title,b.subject_id, b.cost ,								
                            b.percent, b.teacher_name,a.number_student,
                            b.status,b.brief_info,b.subject_id, 
                            b.created_at,b.subject_name,c.number_lesson,
                            a.thumbnail,a.discount				
                    order by 
                  		b.created_at DESC  LIMIT $3 OFFSET $4
            `;
		const param = ['%' + q + '%', subject_id, limit, offset];
		const data = await pool.query(query, param);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getLessonList = async (req, res, next) => {
	const { course_id } = req.query;

	// const userId = req.userData.userId;
	// const role = req.userData.role;
	try {
		// const query_check_course_student=`
		//     select *
		//     from
		//         course_student
		//     where
		//         student_id=$1 and course_id=$2 and flag='false'
		// `
		// const params_check_course_student=[
		//     userId,
		//     course_id
		// ]
		// const check_course_student = await pool.query(query_check_course_student, params_check_course_student);

		const query = `
		select lesson_source.order ,lesson.lesson_order,lesson.lesson_id,lesson.lesson_title,
                source.source_title,source.source_duration, source.source_duration, 
                source.source_link,source.source_detail,source.source_id,lesson_source.order from lesson 
        inner join
            lesson_source 
        on 
            lesson.lesson_id =lesson_source.lesson_id and lesson_source.flag=false  and lesson_source.status='1'
        inner join source  
        on 
            lesson_source.source_id = source.source_id and source.flag=false 
        where course_id = $1 
        order by lesson.lesson_order , lesson_source.order 
		`;
		const param = [course_id];
		const response = await pool.query(query, param);
		const data = response.rows;
		const list_lesson = [];
		let list_source = [];
		let lesson_id = '';
		for (let i = 0; i < data.length; i++) {
			if (lesson_id != data[i].lesson_id) {
				if (list_source.length > 0) {
					const lesson = {
						lesson_id: data[i - 1].lesson_id,
						lesson_title: data[i - 1].lesson_title,
						list_source: list_source,
					};
					list_lesson.push(lesson);
				}

				lesson_id = data[i].lesson_id;
				list_source = [];

				list_source.push({
					source_order: data[i].order,
					source_id: '',
					source_detail: '',
					source_duration: data[i].source_duration,
					source_link: '',
					source_title: data[i].source_title,
				});
			} else {
				list_source.push({
					source_order: data[i].order,
					source_id: '',
					source_detail: '',
					source_duration: data[i].source_duration,
					source_link: '',
					source_title: data[i].source_title,
				});

				// list_source.push({
				//     source_order:data[i].order,
				//     source_id:data[i].source_id,
				//     source_detail:data[i].source_detail,
				//     source_duration:data[i].source_duration,
				//     source_link:data[i].source_link,
				//     source_title:data[i].source_title
				// })
			}
		}
		if (data.length > 0) {
			const lesson = {
				lesson_id: data[data.length - 1].lesson_id,
				lesson_title: data[data.length - 1].lesson_title,
				list_source: list_source,
			};
			list_lesson.push(lesson);
		}

		const result = {
			data: list_lesson,
			total_lesson: data.length,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getLessonSource = async (req, res, next) => {
	const { source_id, lesson_id } = req.query;
	var d = new Date();

	try {
		const query = `
        select 
            source_title,source_link,source_detail,lesson_source.order,lesson.lesson_title
        from 
            source 
        inner join lesson_source
        on 
            lesson_source.source_id = source.source_id
		inner join lesson
			on 
				lesson.lesson_id =  lesson_source.lesson_id
        where source.source_id=$1
        and    lesson_source.lesson_id=$2
		`;
		const param = [source_id, lesson_id];
		const data = await pool.query(query, param);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getSourceIdList = async (req, res, next) => {
	const { course_id, lesson_id } = req.query;
	var d = new Date();
	try {
		const query = `
        select 
            source.source_id
        from 
            source 
        inner join 
            lesson_source
        on 
            lesson_source.source_id = source.source_id
        inner join 
            lesson
        on 
            lesson.lesson_id = lesson_source.lesson_id
            where lesson.course_id=$1
            and lesson.lesson_id =$2 and lesson_source.status='1'
            order by lesson.lesson_order , lesson_source.order 
		`;
		const param = [course_id, lesson_id];
		const data = await pool.query(query, param);
		const listId = [];
		for (let i = 0; i < data.rows.length; i++) {
			listId.push(data.rows[i].source_id);
		}
		const result = {
			data: listId,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getFeatureCourse = async (req, res, next) => {
	try {
		const query = `
        Select  distinct a.course_id as id,b.title,b.subject_name, b.cost as course_cost,								
                b.percent, b.teacher_name,number_student, b.status as course_status, b.brief_info,b.created_at,b.subject_id,
				coalesce(c.number_lesson,0)	as number_lesson,a.thumbnail,a.discount	,b.description, b.short_description,b.avt_link
                from(								
                    select 
                        count(student_id) as number_student, course.course_id,course.thumbnail,course.discount				
                    from 
                        course								
                    left join 
                        course_student on course.course_id = course_student.course_id								
                    group by 
                        course.course_id								
                )a INNER JOIN(								
                    select 
                        course.course_id,course.title,subject.subject_name as subject_name, course.cost ,							
                        course.percent, users.full_name as teacher_name, course.status , course.brief_info, 
                        course.flag, subject.subject_id, course.created_at	,course.description,
                        users.short_description,users.avt_link
                    from 
                        course 
					INNER JOIN subject ON course.subject_id = subject.subject_id	
					INNER JOIN users on users.user_id = course.created_by
                    where course.flag=false and course.is_featured=true
					
                ) b on a.course_id = b.course_id	
					left join (
					select count(lesson_id) as number_lesson,course_id from lesson
								group by course_id
					) c on a.course_id = c.course_id
												
              
            order by 
                created_at DESC 
             offset 0 limit 4
		`;

		const data = await pool.query(query);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getMostBuyCourse = async (req, res, next) => {
	try {
		const query = `
        Select  distinct a.course_id as id,b.title,b.subject_name, b.cost as course_cost,								
        b.percent, b.teacher_name,number_student, b.status as course_status, b.brief_info,b.created_at,b.subject_id,
        coalesce(c.number_lesson,0)	as number_lesson,a.thumbnail,a.discount	,b.description, b.short_description,b.avt_link
        from(								
            select 
                count(student_id) as number_student, course.course_id,course.thumbnail,course.discount				
            from 
                course								
            left join 
                course_student on course.course_id = course_student.course_id								
            group by 
                course.course_id								
        )a INNER JOIN(								
            select 
                course.course_id,course.title,subject.subject_name as subject_name, course.cost ,							
                course.percent, users.full_name as teacher_name, course.status , course.brief_info, 
                course.flag, subject.subject_id, course.created_at	,course.description,
                users.short_description,users.avt_link
            from 
                course 
            INNER JOIN subject ON course.subject_id = subject.subject_id	
            INNER JOIN users on users.user_id = course.created_by
            where course.flag=false
            
        ) b on a.course_id = b.course_id	
            left join (
            select count(lesson_id) as number_lesson,course_id from lesson
                        group by course_id
            ) c on a.course_id = c.course_id
                                    
            order by number_student DESC
            offset 0 limit 4
		`;

		const data = await pool.query(query);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getCourseListByTeacherId = async (req, res, next) => {
	const { page, limit, teacher_id } = req.query;
	const offset = (page - 1) * limit;

	try {
		const query = `
		Select  distinct a.course_id as id,b.title,b.subject_name, b.cost as course_cost,								
                b.percent, b.teacher_name,number_student, b.status as course_status, b.brief_info,b.created_at,b.subject_id,
				coalesce(c.number_lesson,0)	as number_lesson,a.thumbnail,a.discount		
                from(								
                    select 
                        count(student_id) as number_student, course.course_id,course.thumbnail,course.discount				
                    from 
                        course								
                    left join 
                        course_student on course.course_id = course_student.course_id	
                    where course.status ='1'							
                    group by 
                        course.course_id								
                )a INNER JOIN(								
                    select 
                        course.course_id,course.title,subject.subject_name as subject_name, course.cost ,							
                        course.percent, users.full_name as teacher_name, course.status , course.brief_info, 
                        course.flag, subject.subject_id, course.created_at				
                    from 
                        course 
					INNER JOIN subject ON course.subject_id = subject.subject_id	
					INNER JOIN users on users.user_id = course.created_by and users.user_id = $1
                    where course.status ='1'
					
                ) b on a.course_id = b.course_id	
					left join (
					select count(lesson_id) as number_lesson,course_id from lesson
								group by course_id
					) c on a.course_id = c.course_id
												
               where    b.flag=false  
                        
                        						
                group by 
                        a.course_id,b.title,b.subject_id, b.cost ,								
                        b.percent, b.teacher_name,a.number_student,
                        b.status,b.brief_info,b.subject_id, 
                        b.created_at,b.subject_name,c.number_lesson,
                        a.thumbnail,a.discount				
                order by 
               a.course_id , b.created_at  LIMIT $2 OFFSET $3
		`;
		const param = [teacher_id, limit, offset];
		const data = await pool.query(query, param);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

module.exports = {
	getCourseList,
	getLessonList,
	getCourse,
	getLessonSource,
	getSourceIdList,
	getFeatureCourse,
	getMostBuyCourse,
	getCourseListByTeacherId,
};
