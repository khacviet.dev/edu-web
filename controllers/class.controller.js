const pool = require('../configs/db.config');
const HttpError = require('../models/http-error');

const getClasses = async (req, res, next) => {
	const { grade_level, subject, page, limit } = req.query;
	var d = new Date();
	const month_start = ('0' + (((new Date().getMonth() - 3) % 12) + 1)).slice(
		-2
	);
	const month_end = ('0' + (((new Date().getMonth() + 3) % 12) + 1)).slice(-2);
	const year = d.getFullYear();
	const offset = (page - 1) * limit;
	try {
		const query = `
            Select 
                distinct 
                a.class_id,number_student,b.class_name,b.subject_name,b.teacher_name, b.price,b.user_id,									
                b.percent,string_agg(b.room_name , ',') as room_name , b.class_status , b.created_at,b.flag	,
                b.percent,string_agg(b.room_id , ',') as room_id,b.subject_id, b.start_day::character varying,
                string_agg(concat_ws('- ', b.day_of_week::character varying, b.start_time,b.end_time), ',') as day_of_week,b.address,
                b.note
            From (										
                SELECT count(student_id) as number_student,class.class_id										
                FROM class										
                LEFT JOIN class_student										
                ON class.class_id = class_student.class_id										
                group by class.class_id										
                ) a 
            INNER JOIN (										
                SELECT  distinct class.class_id, class.class_name , subject.subject_name ,									
                users.full_name as teacher_name , class.price , class.percent ,  room.room_name , 
                class.class_status ,class.created_at,class.flag,room.room_id,subject.subject_id,
                class.start_day,schedule.day_of_week,slot.start_time,slot.end_time,setting.setting_value as address,
                class.grade_level,class.note,users.user_id
                FROM class INNER JOIN subject ON class.subject_id = subject.subject_id										
                INNER JOIN users   ON class.teacher_id = users.user_id										
                INNER JOIN schedule    ON class.class_id   = schedule.class_id										
                INNER JOIN room on schedule.room_id = room.room_id		
                INNER JOIN slot on slot.slot_id = schedule.slot_id	
                LEFT JOIN setting on setting.setting_id = class.address_id
                order by schedule.day_of_week
                ) b 
            On a.class_id = b.class_id										
            WHERE   b.flag = '0' 
					and b.class_status !='2'
                    and b.subject_id ${subject === '' ? '!' : ''}= $1
                    and b.grade_level${grade_level === '' ? '!' : ''}= $2
                    and (to_char(b.start_day,'MM') >= $3 and to_char(b.start_day,'MM') <= $7 )
                    and to_char(b.start_day,'YYYY')=$4
            Group by    a.class_id,a.number_student,b.class_name, b.subject_name,b.teacher_name, b.price ,										
                        b.percent, b.class_status, b.created_at, b.flag,b.subject_id,b.start_day,b.address,b.note,b.user_id						
            Order by    start_day  DESC LIMIT $5 OFFSET $6
		`;

		const param = [
			subject,
			grade_level,
			month_start,
			year,
			limit,
			offset,
			month_end,
		];

		const data = await pool.query(query, param);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getGradeLevel = async (req, res, next) => {
	try {
		const query = `
            select  distinct grade_level,cast(grade_level as Integer) as level from class
            where grade_level is not null
            order by level
		`;
		const data = await pool.query(query);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

const getSubjectList = async (req, res, next) => {
	try {
		const query = `
        select subject_id, subject_name from subject
		`;
		const data = await pool.query(query);
		const result = {
			data: data.rows,
		};
		return res.status(200).json(result);
	} catch (err) {
		const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
		return next(error);
	}
};

function validatePhoneNumber(input_str) {
	var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
	return re.test(input_str);
}

function validateEmail(email) {
	const re =
		/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

const insertRegister = async (req, res, next) => {
	const { name, email, phone, note, class_id, course_id, user_id } = req.body;
	const checkPhone = validatePhoneNumber(phone);
	const checkEmail = validateEmail(email);
	const id = class_id ? class_id : course_id;
	const today = new Date().toISOString().slice(0, 10);
	if (checkPhone && checkEmail) {
		try {
			const query = `
            INSERT INTO public.register(
                register_id, user_id, ${
									class_id ? 'class_id' : 'course_id'
								}, full_name, email, phone, status,note,date)
                VALUES (nextval('register_id'), $1, $2, $3, $4, $5, $6,$7,$8);
            `;
			const params = [
				user_id ? user_id : null,
				id,
				name,
				email,
				phone,
				0,
				note,
				today,
			];
			await pool.query(query, params);
			const result = {
				message: 'Đăng ký tư vấn thành công !',
			};
			return res.status(200).json(result);
		} catch (err) {
			const error = new HttpError('Đã xảy ra lỗi, vui lòng thử lại.', 500);
			return next(error);
		}
	} else {
		const error = new HttpError(
			'Đầu vào không hợp lệ, vui lòng kiểm tra dữ liệu của bạn.',
			422
		);
		return next(error);
	}
};

module.exports = {
	getClasses,
	getGradeLevel,
	getSubjectList,
	insertRegister,
};
