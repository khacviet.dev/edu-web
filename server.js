require('dotenv').config();

const path = require('path');
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const helmet = require('helmet');
require('./configs/passport.config');
const HttpError = require('./models/http-error');
const fs = require('fs');
const morgan = require('morgan');

const authMiddleware = require('./middlewares/auth.middleware');

const authRoutes = require('./routes/auth.route');
const userRoutes = require('./routes/user.route');
const infoRoutes = require('./routes/info.route');
const classRoutes = require('./routes/class.route');
const courseRoutes = require('./routes/course.route');
const postRoutes = require('./routes/post.route');
const userInfoRoutes = require('./routes/userInfo.route');
const paymentRoutes = require('./routes/payment.route');

const app = express();

app.use(
	helmet({
		contentSecurityPolicy: false,
	})
);

const accessLogStream = fs.createWriteStream(
	path.join(__dirname, '/log/access.log'),
	{ flags: 'a' }
);

app.use(
	morgan(
		':remote-addr [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] - :response-time ms ":user-agent"',
		{
			stream: accessLogStream,
		}
	)
);

const whitelist = ['http://localhost:3000'];

app.use(
	cors({
		origin:
			process.env.ENVIRONMENT === 'prod'
				? '*'
				: (origin, callback) => {
						if (whitelist.indexOf(origin) !== -1) {
							callback(null, true);
						} else {
							callback(new Error('Cors policys.'));
						}
				  },
		methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
		allowedHeaders: 'Authorization,Origin,X-Requested-With,Content-Type,Accept',
		credentials: true,
	})
);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cookieParser());

app.use('/api/v1/auth', authRoutes);
app.use('/api/v1/class', classRoutes);
app.use('/api/v1/course', courseRoutes);
app.use('/api/v1/post', postRoutes);
app.use('/api/v1/info', infoRoutes);
app.use('/api/v1/user', userRoutes);
app.use('/api/v1/userInfo', authMiddleware, userInfoRoutes);
app.use('/api/v1/payment', authMiddleware, paymentRoutes);

if (process.env.ENVIRONMENT === 'prod') {
	app.use(express.static(path.join(__dirname, 'client')));
	app.get('*', (req, res) => {
		res.sendFile(path.join(__dirname + '/client/index.html'));
	});
}

app.use(() => {
	const error = new HttpError('Could not find this route.', 404);
	throw error;
});

app.use((error, req, res, next) => {
	if (res.headersSent) {
		return next();
	}
	res.status(error.code || 500);
	res.json({ message: error.message || 'An unknown error occurred!' });
});

app.listen(process.env.PORT || 4000, () => {
	console.log('Listening on port 4000');
});
