require('dotenv').config();
const router = require('express').Router();
const { check } = require('express-validator');

const authControllers = require('../controllers/auth.controller');

router.post(
	'/signup',
	[
		check('username').isString().notEmpty(),
		check('password').notEmpty().isString().isLength({ min: 6, max: 100 }),
		check('phone')
			.isString()
			.matches(/^[0-9]+$/)
			.isLength({ min: 10, max: 11 }),
		check('fullname').isString().notEmpty(),
		check('email').isEmail().notEmpty(),
	],
	authControllers.signup
);

router.post(
	'/signin',
	[
		check('username').notEmpty().isString(),
		check('password').notEmpty().isString(),
	],
	authControllers.signin
);

router.post(
	'/reset',
	[check('username').notEmpty().isEmail()],
	authControllers.forgotPassword
);

router.post(
	'/reset/recover/:userId',
	[check('code').notEmpty().isInt()],
	authControllers.checkRecoverCode
);

router.post(
	'/reset/newpassword/:userId',
	[check('newpassword').notEmpty().isString().isLength({ min: 6, max: 100 })],
	authControllers.setNewPassword
);

router.post(
	'/confirm',
	[check('email').notEmpty().isEmail(), check('code').notEmpty().isString()],
	authControllers.checkConfirmCode
);

router.post('/logout', authControllers.logout);

module.exports = router;
