const express = require('express');
const router = express.Router();

const paymentController = require('../controllers/payment.controller');

router.get('/courseByCourseCart', paymentController.getCourseByCourseCart);
router.post('/', paymentController.paymentCourse);

module.exports = router;
