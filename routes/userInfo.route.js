const express = require('express');
const router = express.Router();
const { check } = require('express-validator');

const userInfoController = require('../controllers/userInfo.controller');

router.get('/getUserInfo', userInfoController.getUserInfo);

router.get('/getCourseList', userInfoController.getCourseList);

router.get('/getLessonList', userInfoController.getLessonList);

router.post(
	'/sendFeedBack',
	[
		check('courseId').isString().notEmpty(),
		check('feedback').isString().notEmpty(),
	],
	userInfoController.sendFeedBack
);
router.get('/checkIsBuy', userInfoController.checkIsBuy);




module.exports = router;
