const express = require('express');
const router = express.Router();

const postController = require('../controllers/post.controller');

router.get('/getFourNewPost', postController.getFourNewPost);

router.get('/getPostCategory', postController.getPostCategory);

router.get('/getPosts', postController.getPosts);

router.get('/getPostById', postController.getPostById);

router.get('/getPostDepend', postController.getPostDepend);

router.post('/updateClick', postController.updateClick);

module.exports = router;
