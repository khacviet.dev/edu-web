const express = require('express');
const router = express.Router();

const courseController = require('../controllers/course.controller');

router.get('/getCourseList', courseController.getCourseList);

router.get('/getLessonList', courseController.getLessonList);

router.get('/getCourse', courseController.getCourse);

router.get('/getLessonSource', courseController.getLessonSource);

router.get('/getSourceIdList', courseController.getSourceIdList);

router.get('/getFeatureCourse', courseController.getFeatureCourse);

router.get('/getMostBuyCourse', courseController.getMostBuyCourse);

router.get(
	'/getCourseListByTeacherId',
	courseController.getCourseListByTeacherId
);

module.exports = router;
