const express = require('express');
const router = express.Router();

const classController = require('../controllers/class.controller');

router.get('/getClasses', classController.getClasses);

router.get('/getGradeLevel', classController.getGradeLevel);

router.get('/getSubjectList', classController.getSubjectList);

router.post('/insertRegister', classController.insertRegister);

module.exports = router;
