const express = require('express');
const router = express.Router();

const infoController = require('../controllers/info.controller');

router.get('/getSliders', infoController.getSliders);
router.get('/getFooter', infoController.getFooter);

module.exports = router;
